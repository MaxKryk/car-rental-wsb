package com.example.carrental.controller;

import com.example.carrental.classes.Car.Car;
import com.example.carrental.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class PreloadCarDB {
    @Bean //zadziala przy uruchoieniu aplikacji
    public CommandLineRunner demo (CarRepository repository){
        return (args)->{
            //save some cars as example
            log.info( "Preloading " + repository.save(new Car("ford mustang","ford", "mustang", "mustang.jpg", "Ford Mustang z roku 2012 z silnikiem 3.7 V6 o mocy 305KM", 300.0, 550.0, 1300.0)) );
           // log.info( "Preloading " + repository.save(new Car("ford focus1","ford", "fiesta", "car.jpg", "data", 23.0)) );
//            log.info( "Preloading " + repository.save(new Car("ford focus2","volvo", "fiesta", "car.jpg", "data", 23.0)) );
//            log.info( "Preloading " + repository.save(new Car("ford focus3","ford", "escape", "car.jpg", "data", 23.0)) );
//            log.info( "Preloading " + repository.save(new Car("ford focus4", "ford", "escape", "car.jpg", "data", 23.0)));
        log.info( repository.findById( new Long (1) ).toString() );
        };

}}
