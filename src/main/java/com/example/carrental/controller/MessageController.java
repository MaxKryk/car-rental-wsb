package com.example.carrental.controller;

import com.example.carrental.classes.InfoDTO;
import com.example.carrental.classes.Mail.EmailService;
import com.example.carrental.classes.Message.Message;
import com.example.carrental.repository.MessageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@Slf4j
public class MessageController {
    @Autowired
    private EmailService emailService;
    private final MessageRepository messageRepository;
MessageController(MessageRepository messageRepository) {this.messageRepository = messageRepository;}

    @PostMapping("/send_message")
    public ModelAndView sendMessage(@ModelAttribute Message message, Model model){
        messageRepository.save( message );
        log.info("start mail from controller");

        emailService.sendMail("carrentalwsbcarrentalwsb@gmail.com", message.getMail() + ": " +message.getSubject(), message.toString());
        log.info("Mail from controller has been sent");

        InfoDTO infoDTO=new InfoDTO();
        model.addAttribute( "infoDto",  infoDTO);
        infoDTO.setMessage( "Email został wysłany! Odpowiemy na niego tak szybko, jak to możliwe. Twoja wiadomość: \n"+message.toString() );
        return new ModelAndView("info");}}



//    public ModelAndView sendMessage(@ModelAttribute Message message){//, Model model){
//        log.info("start mail from controller");
//messageRepository.save( message );
//
//
//        emailService.sendMail("yourdreamclub809@gmail.com", message.getMail() + ": " +message.getSubject(), message.toString());
//        log.info("Mail from controller has been sent");
////        InfoDTO infoDTO = new InfoDTO();
////        model.addAttribute( "infoDTO", infoDTO );
////        infoDTO.setMessage( "Wiadomość wysłana!" );
//        return new ModelAndView("info");
//    }

