package com.example.carrental.controller;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {

http
        .csrf().disable()
        .authorizeRequests()
        .antMatchers(HttpMethod.POST, "/add_new_car/**").hasRole("ADMIN")
        .antMatchers(HttpMethod.POST, "/find_car/**").hasRole("ADMIN")
        .antMatchers(HttpMethod.POST, "/update_car_info/**").hasRole("ADMIN")
                .antMatchers("/admin_panel").hasRole("ADMIN")
        //.authenticated()//Makes / and /index to be authenthicated
//        .antMatchers("/", "/admin_panel", "/home_test").authenticated()//Makes / and /index to be authenthicated
        .anyRequest().permitAll()//Makes any other allow without authentication. Or if you want a group use antMatchers here too.
        .and()
        .formLogin()
        .loginPage("/login")
        .permitAll()
        .and()
        .logout()
        .permitAll()
;
    }



    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**", "/static/**", "/img/**", "/css/**", "/js/**", "/vendor/**", "/scss/**", "/img/**", "/icon/**","/webjars/**");
    }

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        UserDetails user =
                User.withDefaultPasswordEncoder()
                        .username("admin")
                        .password("acvb12bacx34")
                        .roles("ADMIN")
                        .build();

        return new InMemoryUserDetailsManager(user);
    }
}