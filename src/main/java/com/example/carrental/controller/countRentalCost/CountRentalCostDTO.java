package com.example.carrental.controller.countRentalCost;

import lombok.Data;

@Data
public class CountRentalCostDTO {
    private String carName;
    private String startReservationDate;
    private String endReservationDate;
}
