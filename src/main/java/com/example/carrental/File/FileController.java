package com.example.carrental.File;

import com.example.carrental.classes.Car.Car;
import com.example.carrental.classes.Reservation.Picname;
import com.example.carrental.classes.Reservation.Reservation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;

@Slf4j
@Controller

public class FileController {


    @Autowired
    FileService fileService;
//
//    @GetMapping("/")
//    public String index() {
//        return "upload";
//    }
//
//

//    @PostMapping("/uploadFile")
//    public @ResponseBody void uploadFile(Model model, @RequestParam("file") MultipartFile multipartFile) throws InterruptedException {
//        //, RedirectAttributes redirectAttributes) throws InterruptedException {
//Reservation reservation=new Reservation(  );
//    fileService.uploadFile( multipartFile );
//    File file = new File("\\car-rental\\src\\main\\resources\\static\\attachments", multipartFile.getOriginalFilename());
//log.info( "name and path " + file.getName() + file.getPath() );
//
//Picname picname=new Picname();
//            picname.setPicnameAsString(file.getName() );
//
//log.info( "picname file " + picname.getPicnameAsString() );
//
//        TimeUnit.SECONDS.sleep(2);
//        //return "redirect:/add_new_car";
//        }
    @PostMapping("/uploadFile")
//    public ResponseEntity<?> uploadFile(Model model, @RequestParam("file") MultipartFile multipartFile) throws InterruptedException {
    public String uploadFile(@ModelAttribute Picname picname, Model model, @ModelAttribute Car car, @RequestParam("file") MultipartFile multipartFile, RedirectAttributes redirectAttributes)  {

        Reservation reservation=new Reservation(  );
        fileService.uploadFile( multipartFile );
        File file = new File("\\car-rental\\src\\main\\resources\\static\\attachments", multipartFile.getOriginalFilename());
        log.info( "name and path " + file.getName() + file.getPath() );

        //Picname picname=new Picname();
        picname.setPicnameAsString(file.getName() );

        log.info( "picname file " + picname.getPicnameAsString() );

//            return ResponseEntity.ok().build();
//        }catch(Exception e){
//            return ResponseEntity.badRequest().build();
//        }
        model.addAttribute( "picname", picname );
            return new String("redirect:/add_new_car2");
    }}


