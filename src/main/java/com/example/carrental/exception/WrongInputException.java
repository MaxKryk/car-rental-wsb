package com.example.carrental.exception;

public class WrongInputException extends RuntimeException{
    public WrongInputException(){super("Wrong input type");}}