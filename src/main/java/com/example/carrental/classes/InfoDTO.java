package com.example.carrental.classes;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@NoArgsConstructor
public class InfoDTO {
    @Id
    @GeneratedValue
    long id;
    private String message="nieznany bład";
}
