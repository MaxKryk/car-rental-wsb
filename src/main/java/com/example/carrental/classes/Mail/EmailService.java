package com.example.carrental.classes.Mail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Service("emailService")
public class EmailService
{
    @Autowired
    private JavaMailSender mailSender;

    private SimpleMailMessage preConfiguredMessage;

  //sends mail
    public void sendMail(String to, String subject, String body)
    {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(body);
        mailSender.send(message);
    }

//send predefined message
    public void sendPreConfiguredMail(String message)
    {
        SimpleMailMessage mailMessage = new SimpleMailMessage(preConfiguredMessage);
        mailMessage.setText(message);
        mailSender.send(mailMessage);
    }
//sends mail with attachment
    public void sendMailWithAttachment(String to, String subject, String body, String filepathToAttach)
    {
        MimeMessagePreparator preparator = new MimeMessagePreparator()
        {
            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setRecipient( Message.RecipientType.TO, new InternetAddress( to ) );
                mimeMessage.setFrom( new InternetAddress( "yourdreamclub809@gmail.com" ) );
                mimeMessage.setSubject( subject );
                mimeMessage.setText( body );

                Multipart multipart = new MimeMultipart();
                MimeBodyPart attachmentPart = new MimeBodyPart();
                File file=new File( filepathToAttach );
                try {
                    attachmentPart.attachFile( file);

                    multipart.addBodyPart( attachmentPart );
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mimeMessage.setContent( multipart );

            }};

        try {
            mailSender.send(preparator);
            log.info("after mailSender.send(preparator);");
        }
        catch (MailException ex) {
            // simply log it and go on...
            System.err.println(ex.getMessage());
        }
    }


    //sends mail with attachment
    public void sendMailWithAttachmentGivenAsFile(String to, String subject, String body, File file)
    {
        MimeMessagePreparator preparator = new MimeMessagePreparator()
        {
            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setRecipient( Message.RecipientType.TO, new InternetAddress( to ) );
                mimeMessage.setFrom( new InternetAddress( "yourdreamclub809@gmail.com" ) );
                mimeMessage.setSubject( subject );
                mimeMessage.setText( body );

                Multipart multipart = new MimeMultipart();
                MimeBodyPart attachmentPart = new MimeBodyPart();
                try {
                    attachmentPart.attachFile( file);

                    multipart.addBodyPart( attachmentPart );
                } catch (IOException e) {
                    e.printStackTrace();
                }
                mimeMessage.setContent( multipart );

            }};

        try {
            mailSender.send(preparator);
            log.info("after mailSender.send(preparator);");
        }
        catch (MailException ex) {
            // simply log it and go on...
            System.err.println(ex.getMessage());
        }
    }}


