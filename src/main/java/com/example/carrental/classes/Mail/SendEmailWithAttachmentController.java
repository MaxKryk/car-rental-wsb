package com.example.carrental.classes.Mail;

import com.example.carrental.classes.InfoDTO;
import com.example.carrental.classes.Reservation.Reservation;
import com.example.carrental.repository.ReservationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;

@Slf4j
@Controller

public class SendEmailWithAttachmentController {
    @Autowired
    private JavaMailSender mailSender;
    private final ReservationRepository reservationRepository;
    SendEmailWithAttachmentController(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @PostMapping("/sendEmailWithFile")
    public ModelAndView sendEmailWithFile(Model model, HttpServletRequest request, final @RequestParam MultipartFile attachFile

    ){
            log.info( "beginning" );
//
              String      mail                       = request.getParameter("mail");
              String      name                       = request.getParameter("name");
              String      car                        = request.getParameter("car");
              String      startReservationDate       = request.getParameter("startReservationDate");
              String      endReservationDate         = request.getParameter("endReservationDate");
              String      file                       = request.getParameter("file");

              log.info("  mail                   " + mail                   );
              log.info("  name                   " + name                   );
              log.info("  car                    " + car                    );
              log.info("  startReservationDate   " + startReservationDate   );
              log.info("  endReservationDate     " + endReservationDate     );
              log.info("  file                   " + file                   );

Reservation reservation = new Reservation( mail, name, car, startReservationDate, endReservationDate, file );
reservationRepository.save( reservation );
log.info( "reserv to String " + reservation.toString() );
        final String emailTo = "yourdreamclub809@gmail.com";
        final String subject = reservation.getMail() + ": Prośba o rezerwację";
        final String message = (reservation.toMail());

        //final String emailTo = request.getParameter("mailTo");
        //final String subject = request.getParameter("subject");
        //final String message = request.getParameter("message");

        log.info("mailTo, subject, message");
        log.info("emailTo" + emailTo);
        log.info("subject" + subject);
        log.info("message" + subject);


        log.info("attachFile: " + attachFile.getOriginalFilename());

        mailSender.send(new MimeMessagePreparator() {

            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper messageHelper = new MimeMessageHelper(
                        mimeMessage, true, "UTF-8");

                messageHelper.setTo(emailTo);
                messageHelper.setSubject(subject);
                messageHelper.setText(message);


                // determines if there is an upload file, attach it to the e-mail
                String attachName = attachFile.getOriginalFilename();
                if (!attachFile.equals("")) {

                    messageHelper.addAttachment(attachName, new InputStreamSource() {

                        @Override
                        public InputStream getInputStream() throws IOException {
                            return attachFile.getInputStream();
                        }
                    });
                }

            }
        });
        InfoDTO infoDTO=new InfoDTO();
model.addAttribute( "infoDto",  infoDTO);
infoDTO.setMessage( "Rezerwacja została wysłana! Odpowiemy na nią tak szybko, jak to możliwe." );
        return new ModelAndView("info");}}