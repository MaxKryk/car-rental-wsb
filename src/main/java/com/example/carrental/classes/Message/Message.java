package com.example.carrental.classes.Message;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.logging.Logger;

@Data
@Entity
@NoArgsConstructor
public class Message {

    @Id
    @GeneratedValue long id;
    private String name;
    private String mail;
    private String subject;
    private String content;

    @Override
    public String toString() {
        return "Wiadomość z formularza kontaktowego\n" +
                "id=" + id +
                ", \nOd:'" + name + '\'' +
                ", \nMail:'" + mail + '\'' +
                ", \nTemat:'" + subject + '\'' +
                ", \nTreść:'" + content + '\'' +
                '}';
    }
}

