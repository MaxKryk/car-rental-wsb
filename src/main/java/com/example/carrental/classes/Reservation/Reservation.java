package com.example.carrental.classes.Reservation;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.File;
@Data
@Entity
@NoArgsConstructor
public class Reservation {
    @Id
    @GeneratedValue
    long id;
    private String mail;
    private String name;
    private String car;
    private String startReservationDate;
    private String endReservationDate;
    private String file;


    public Reservation(String mail, String name, String car, String startReservationDate, String endReservationDate, String file) {
        this.mail = mail;
        this.name = name;
        this.car = car;
        this.startReservationDate = startReservationDate;
        this.endReservationDate = endReservationDate;
        this.file = file;
    }


    public String toMail() {
        String newline=System.getProperty( "line.separator" );
        return "Witaj, Radosławie! Właśnie dostałeś nową prośbę o rezerwację!" + newline+
                "Prośba o rezerwację: " + newline +
                "id: " + id + newline +
                "mail: " + mail + newline +
                "imię i nazwisko:'" + name + newline +
                "auto:" + car.toString() + newline +
                "rezerwacja od:" + startReservationDate + newline +
                "rezerwacja do:" + endReservationDate + newline
                ;
    }
}
